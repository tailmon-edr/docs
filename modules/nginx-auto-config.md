
# Nginx不停机更新配置

## 需求

流量监控需要明文的流量数据，这种情况下，对于https加密流量，通常的做法是通过代理方式，例如nginx，在nginx上部署https，然后将流量解密后通过本地地址转发到后端应用，流量监控程序监控本地网卡即可。这就需要代理软件需要正确配置，特别是重要的信息，例如真实客户端IP地址X-Forwarded-For，以免流量监控找不到真实的攻击源IP。

修改配置不能影响Nginx正常运行，因此需要不停机更新配置。

## 修改配置

在不停止Nginx服务的情况下平滑变更Nginx配置

修改/usr/local/webserver/nginx/conf/nginx.conf配置文件后，请执行以下命令检查配置文件是否正确：

```
/usr/local/webserver/nginx/sbin/nginx -t
```

如果屏幕显示以下两行信息，说明配置文件正确：

```
the configuration file /usr/local/webserver/nginx/conf/nginx.conf syntax is ok
the configuration file /usr/local/webserver/nginx/conf/nginx.conf was tested successfully
```

## 平滑重启

对于Nginx 0.8.x版本，现在平滑重启Nginx配置非常简单，执行以下命令即可：

```
/usr/local/webserver/nginx/sbin/nginx -s reload
```

对于Nginx 0.8.x之前的版本，平滑重启稍微麻烦一些，按照以下步骤进行即可。输入以下命令查看Nginx主进程号：

```
ps -ef | grep "nginx: master process" | grep -v "grep" | awk -F ' ' '{print $2}'
```

屏幕显示的即为Nginx主进程号，例如：6302

这时，执行以下命令即可使修改过的Nginx配置文件生效：

```
kill -HUP 6302
```

或者无需这么麻烦，找到Nginx的Pid文件：

```
kill -HUP `cat /usr/local/webserver/nginx/nginx.pid`
```