# 路线图

## Tailmon主机安全卫士

### v1

- 基础架构
- 数据规范
- 稳定性
- 易用性
- 规则全面性精准性
- 我的主机（运行监控）
- 实时防护（Suricata、Falco）
- 病毒查杀（Yara）
- 响应处置（删文件、杀进程、阻断IP）
- 设置

### v2

- 漏洞检测（漏洞、基线、弱口令）
- 应用管理（资产扫描、梳理）

### v3

- 异常行为监控（原始流量、原始日志、原始内核调用）

### v4

- 基于Falco实现容器化环境（Docker、Kubernetes）下安全监控

## Tailmon安全运营中心

### v1

- 管理多个Tailmon主机安全卫士