-- SQLite数据库

drop table if exists `user`;
create table `user`
(
    `id` integer primary key autoincrement, --用户ID
    `username` text not null unique, --用户名。不可重复
    `password` text not null, --密码
    `allow_multi_device` integer not null, --允许设备上限数。e.g. 3表示该账户允许最多在3台设备登录
    `allow_multi_online` integer not null, --是否允许多设备同时在线。0代表不允许，1代表允许
    `create_date` integer not null, --用户注册日期
    `update_date` integer not null, --用户更新日期
    `status` integer not null --用户账号状态，-1删除，0禁用，1启用
);

insert into `user` values (1, 'admin001', 'MD5', 1, 1, 1650251557542, 1650251557542, 1);

drop table if exists `role`;
create table `role`
(
    `id` text primary key, --角色id，e.g. admin
    `name` text not null --角色名称
);

insert into `role` values ('admin', '管理员');

drop table if exists `user_role`;
create table `user_role`
(
    `user_id` integer, --用户ID
    `role_id` text, --角色ID
    primary key (`user_id`, `role_id`)
);

insert into `user_role` values (1, 'admin');

-- permission redis缓存
drop table if exists `permission`;
create table `permission`
(
    `id` integer primary key autoincrement, --权限ID
    `module` text not null, --模块。e.g. user
    `action` text null, --操作。e.g. list
    `desc` text not null --描述。e.g. 告警事件
);


drop table if exists `role_permission`;
create table `role_permission`
(
    `role_id` text, --角色ID
    `permission_id` integer, --权限ID
    primary key (`role_id`, `permission_id`)
);

insert into
    `permission`
values
    (1, 'alert', null, '告警事件'),
    (2, 'alert', 'list', '查询'),
    (3, 'alert', 'insert', '添加'),
    (4, 'alert', 'update', '修改'),
    (5, 'alert', 'delete', '删除'),

    (6, 'agent-config', null, '终端配置'),
    (7, 'agent-config', 'list', '查询'),
    (8, 'agent-config', 'insert', '添加'),
    (9, 'agent-config', 'update', '修改'),
    (10, 'agent-config', 'delete', '删除'),

    (11, 'agent-status', null, '终端状态'),
    (12, 'agent-status', 'list', '查询'),
    (13, 'agent-status', 'insert', '添加'),
    (14, 'agent-status', 'update', '修改'),
    (15, 'agent-status', 'delete', '删除'),

    (16, 'dashboard', null, '控制台'),
    (17, 'dashboard', 'list', '查询'),
    (18, 'dashboard', 'insert', '添加'),
    (19, 'dashboard', 'update', '修改'),
    (20, 'dashboard', 'delete', '删除'),

    (21, 'update-package', null, '终端升级'),
    (22, 'update-package', 'list', '查询'),
    (23, 'update-package', 'insert', '添加'),
    (24, 'update-package', 'update', '修改'),
    (25, 'update-package', 'delete', '删除'),

    (26, 'user', null, '用户管理'),
    (27, 'user', 'list', '查询'),
    (28, 'user', 'insert', '添加'),
    (29, 'user', 'update', '修改'),
    (30, 'user', 'delete', '删除'),

    (31, 'role', null, '角色管理'),
    (32, 'role', 'list', '查询'),
    (33, 'role', 'insert', '添加'),
    (34, 'role', 'update', '修改'),
    (35, 'role', 'delete', '删除'),

    (36, 'permission', null, '权限管理'),
    (37, 'permission', 'list', '查询'),
    (38, 'permission', 'insert', '添加'),
    (39, 'permission', 'update', '修改'),
    (40, 'permission', 'delete', '删除');


insert into `role_permission` values ('admin', 1),
                                     ('admin', 2),
                                     ('admin', 3),
                                     ('admin', 4),
                                     ('admin', 5),
                                     ('admin', 6),
                                     ('admin', 7),
                                     ('admin', 8),
                                     ('admin', 9),
                                     ('admin', 10),
                                     ('admin', 11),
                                     ('admin', 12),
                                     ('admin', 13),
                                     ('admin', 14),
                                     ('admin', 15),
                                     ('admin', 16),
                                     ('admin', 17),
                                     ('admin', 18),
                                     ('admin', 19),
                                     ('admin', 20),
                                     ('admin', 21),
                                     ('admin', 22),
                                     ('admin', 23),
                                     ('admin', 24),
                                     ('admin', 25),
                                     ('admin', 26),
                                     ('admin', 27),
                                     ('admin', 28),
                                     ('admin', 29),
                                     ('admin', 30),
                                     ('admin', 31),
                                     ('admin', 32),
                                     ('admin', 33),
                                     ('admin', 34),
                                     ('admin', 35),
                                     ('admin', 36),
                                     ('admin', 37),
                                     ('admin', 38),
                                     ('admin', 39),
                                     ('admin', 40);
/*
流程：

请求拦截：

获取Cookie User Access-Token => afsdfjldsfjasdlkfjasldkfjkfjklsd

通过Access-Token获取用户信息，重要的是用户权限id列表（此处有redis缓存）

获取URL

e.g.

/alert/list/security

获取module和action（命名规范重要）

module => alert
action => list

通过module, action获取权限id（此处有redis缓存）

判断用户权限id列表是否有该权限id，有：放行

*/