# 开发环境搭建

## CentOS7

### 安装yara

```

yum install -y automake
yum install -y libtool
yum install -y make
yum install -y gcc
yum install -y pkgconfig
yum install -y file-libs
yum install -y file-devel
yum install -y openssl-devel
yum install jansson-devel

yum install -y clang-devel


cd yara-4.2.1
./bootstrap.sh

./configure --enable-cuckoo --enable-magic --enable-dotnet --with-crypto

make

sudo make install
```

### 安装Clang3.9

```
yum -y install epel-release
```

```
vim /etc/yum.repos.d/epel.repo
```
添加如下内容
```
[alonid-llvm-3.9.0]
name=Copr repo for llvm-3.9.0 owned by alonid
baseurl=https://copr-be.cloud.fedoraproject.org/results/alonid/llvm-3.9.0/epel-7-$basearch/
type=rpm-md
skip_if_unavailable=True
gpgcheck=1
gpgkey=https://copr-be.cloud.fedoraproject.org/results/alonid/llvm-3.9.0/pubkey.gpg
repo_gpgcheck=0
enabled=1
enabled_metadata=1
```

安装

```
yum install clang-3.9.0
```

配置环境变量


```
vim /etc/profile
```

添加如下内容

```
export PATH=/opt/llvm-3.9.0/bin:$PATH
export LD_LIBRARY_PATH=/opt/llvm-3.9.0/lib64:$LD_LIBRARY_PATH
```
```
source /etc/profile
```


### 编译tailmonscan

```
cargo build --release
```

依赖动态链接库

```
linux-vdso.so.1 =>  (0x00007ffeea16d000)
libyara.so.9 => /usr/local/lib/libyara.so.9 (0x00007f5a6ca8d000)
libcrypto.so.10 => /lib64/libcrypto.so.10 (0x00007f5a6c62a000)
libmagic.so.1 => /lib64/libmagic.so.1 (0x00007f5a6c40d000)
libjansson.so.4 => /lib64/libjansson.so.4 (0x00007f5a6c200000)
libm.so.6 => /lib64/libm.so.6 (0x00007f5a6befe000)
libpthread.so.0 => /lib64/libpthread.so.0 (0x00007f5a6bce2000)
libc.so.6 => /lib64/libc.so.6 (0x00007f5a6b914000)
libdl.so.2 => /lib64/libdl.so.2 (0x00007f5a6b710000)
libz.so.1 => /lib64/libz.so.1 (0x00007f5a6b4fa000)
/lib64/ld-linux-x86-64.so.2 (0x00007f5a6ccf7000)
```

### 编译tailmond


```
cargo build --release
```

依赖动态链接库

```
linux-vdso.so.1 =>  (0x00007ffdfada2000)
libstdc++.so.6 => /lib64/libstdc++.so.6 (0x00007fddb382f000)
libsqlite3.so.0 => /lib64/libsqlite3.so.0 (0x00007fddb357a000)
libgcc_s.so.1 => /lib64/libgcc_s.so.1 (0x00007fddb3364000)
librt.so.1 => /lib64/librt.so.1 (0x00007fddb315c000)
libpthread.so.0 => /lib64/libpthread.so.0 (0x00007fddb2f40000)
libm.so.6 => /lib64/libm.so.6 (0x00007fddb2c3e000)
libdl.so.2 => /lib64/libdl.so.2 (0x00007fddb2a3a000)
libc.so.6 => /lib64/libc.so.6 (0x00007fddb266c000)
/lib64/ld-linux-x86-64.so.2 (0x00007fddb516f000)
```
