
# suricata 动态库打包

## 全

```
cp -r /lib64/liblua-5.1.so* ./
cp -r /lib64/libhtp.so.2* ./
cp -r /lib64/librt.so.1* ./
cp -r /lib64/librt-2.17.so ./
cp -r /lib64/libm.so.6* ./
cp -r /lib64/libm-2.17.so ./
cp -r /lib64/liblz4.so.1* ./
cp -r /lib64/libevent_pthreads-2.0.so.5* ./
cp -r /lib64/libpthread-2.17.so ./
cp -r /lib64/libevent-2.0.so.5* ./
cp -r /lib64/libhiredis.so.0.12* ./
cp -r /lib64/libmaxminddb.so.0* ./
cp -r /lib64/libmagic.so.1* ./
cp -r /lib64/libcap-ng.so.0* ./
cp -r /lib64/libpcap.so.1* ./
cp -r /lib64/libnet.so.1* ./
cp -r /lib64/libnetfilter_queue.so.1* ./
cp -r /lib64/libnfnetlink.so.0* ./
cp -r /lib64/libjansson.so.4* ./
cp -r /lib64/libyaml-0.so.2* ./
cp -r /lib64/libpcre.so.1* ./
cp -r /lib64/libz.so.1* ./
cp -r /lib64/libssl3.so* ./
cp -r /lib64/libsmime3.so* ./
cp -r /lib64/libnss3.so* ./
cp -r /lib64/libnssutil3.so* ./
cp -r /lib64/libplds4.so* ./
cp -r /lib64/libplc4.so* ./
cp -r /lib64/libnspr4.so* ./
cp -r /lib64/libpthread.so.0* ./
cp -r /lib64/libdl.so.2* ./
cp -r /lib64/libgcc_s.so.1* ./
cp -r /lib64/libmnl.so.0* ./
cp -r /lib64/libdl-2.17.so ./
cp -r /lib64/libgcc_s-4.8.5-20150702.so.1 ./
```

## 标准

```
cp -r /lib64/liblua-5.1.so* ./
cp -r /lib64/libhtp.so.2* ./
cp -r /lib64/liblz4.so.1* ./
cp -r /lib64/libevent_pthreads-2.0.so.5* ./
cp -r /lib64/libpthread-2.17.so ./
cp -r /lib64/libevent-2.0.so.5* ./
cp -r /lib64/libhiredis.so.0.12* ./
cp -r /lib64/libmaxminddb.so.0* ./
cp -r /lib64/libmagic.so.1* ./
cp -r /lib64/libcap-ng.so.0* ./
cp -r /lib64/libpcap.so.1* ./
cp -r /lib64/libnet.so.1* ./
cp -r /lib64/libnetfilter_queue.so.1* ./
cp -r /lib64/libnfnetlink.so.0* ./
cp -r /lib64/libjansson.so.4* ./
cp -r /lib64/libyaml-0.so.2* ./
cp -r /lib64/libpcre.so.1* ./
cp -r /lib64/libssl3.so* ./
cp -r /lib64/libsmime3.so* ./
cp -r /lib64/libnss3.so* ./
cp -r /lib64/libnssutil3.so* ./
cp -r /lib64/libplds4.so* ./
cp -r /lib64/libplc4.so* ./
cp -r /lib64/libnspr4.so* ./
cp -r /lib64/libpthread.so.0* ./
cp -r /lib64/libmnl.so.0* ./
```

# tailmonscan 动态库打包

## 全

```
cp -r /usr/local/lib/libyara.so.9 (0x00007f5a6ca8d000)
cp -r /lib64/libcrypto.so.10 (0x00007f5a6c62a000)
cp -r /lib64/libmagic.so.1 (0x00007f5a6c40d000)
cp -r /lib64/libjansson.so.4 (0x00007f5a6c200000)
cp -r /lib64/libm.so.6 (0x00007f5a6befe000)
cp -r /lib64/libpthread.so.0 (0x00007f5a6bce2000)
cp -r /lib64/libc.so.6 (0x00007f5a6b914000)
cp -r /lib64/libdl.so.2 (0x00007f5a6b710000)
cp -r /lib64/libz.so.1 (0x00007f5a6b4fa000)


libgcc_s.so.1 => /lib64/libgcc_s.so.1 (0x00007fa28eca7000)
librt.so.1 => /lib64/librt.so.1 (0x00007fa28ea9f000)
cp -r /lib64/libcrypto.so.1.0.2k ./
cp -r /lib64/libpthread-2.17.so ./
```

## 标准

```
cp -r /usr/local/lib/libyara.so.9* ./
cp -r /lib64/libcrypto.so.10* ./
cp -r /lib64/libmagic.so.1* ./
cp -r /lib64/libjansson.so.4* ./
cp -r /lib64/libpthread.so.0* ./
cp -r /lib64/libcrypto.so.1.0.2k ./
cp -r /lib64/libpthread-2.17.so ./
```