

## 经典代理模式


配置

```
vim Ca
```

```

{
  admin off # 关闭管理界面
}

:8080 {
    root * /root/myblog
    file_server
}


jthinking.com {
    reverse_proxy localhost:8080
}

```

```
./caddy start  --config ./Caddyfile
```